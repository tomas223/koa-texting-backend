import Router from "koa-router";
import { loginUser } from "./controllers/auth";
import { createMessage, getStats } from "./controllers/message";
import { authenticated, authenticatedAdmin } from "./middleware/authenticated";
import { messageValidator } from "./middleware/validators";

const router = new Router();

router.get("/stats", authenticatedAdmin, getStats);

router.post("/message", authenticated, messageValidator, createMessage);

router.post("/login", loginUser);

export const routes = router.routes();
