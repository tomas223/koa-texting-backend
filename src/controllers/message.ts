import { Context } from "koa";

import Message from "../models/message";

const getStats = async (ctx: Context) => {
  const { count, value } = await Message.getLast();


  ctx.status = 200;
  ctx.body = { lastMessage: value, numberOfCalls: count };
};

const createMessage = async (ctx: Context) => {
  const { message } = ctx.request.body;

  await Message.saveLast(message);

  ctx.status = 201;
  ctx.body = ctx.request.body;
};

export { getStats, createMessage };
