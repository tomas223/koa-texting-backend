import { Context, Next } from "koa";
import jwt from "jsonwebtoken";

import { comparePassword } from "../utils/bcrypt";
import User from "../models/user";
import { config } from "../config";

const WRONG_AUTH_MGS = "Incorrect username and/or password.";

const loginUser = async (ctx: Context) => {
  const { username, password } = ctx.request.body;

  if (!username) ctx.throw(422, "Username required.");
  if (!password) ctx.throw(422, "Password required.");

  const dbUser = User.findByUsername(username);

  if (!dbUser) ctx.throw(401, WRONG_AUTH_MGS);

  if (comparePassword(password, dbUser.passwordHash)) {
    const payload = { sub: dbUser.id };
    const token = jwt.sign(payload, config.jwtSecret);

    ctx.body = { accessToken: token };
  } else {
    ctx.throw(401, WRONG_AUTH_MGS);
  }
};

export { loginUser };
