import Koa from "koa";
import koaBody from "koa-body";
// import passport from "koa-passport";

import { config } from "./config";
import { routes } from "./routes";
import errorHandler from "./middleware/errorHandler";

const app = new Koa();

app.use(errorHandler);
app.use(koaBody());
// app.use(passport.initialize());
app.use(routes);

app.on("error", (err) =>
  console.log(`API Error occured (${err.status}): ${err.message}`)
);

app.listen(config.port);

console.log(`Server running on port ${config.port}`);
