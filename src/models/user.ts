// password is not hashed for this demo

type IUser = {
  id: number;
  username: string;
  passwordHash: string;
  role: "admin" | "user";
};

const USERS: IUser[] = [
  { id: 1, username: "admin", passwordHash: "secret", role: "admin" },
  { id: 2, username: "user", passwordHash: "secret", role: "user" },
];

const User = {
  findById: (id: number): IUser | undefined =>
    USERS.find((user) => user.id === id),

  findByUsername: (username: string): IUser | undefined =>
    USERS.find((user) => user.username === username),

  isAdmin: (userId: number): boolean => User.findById(userId)?.role === "admin",
};

export default User;
