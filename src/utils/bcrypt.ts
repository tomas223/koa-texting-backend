const comparePassword = (password: string, hashedPassword: string) =>
  password === hashedPassword;

export { comparePassword };
