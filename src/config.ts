import dotenv from "dotenv";

dotenv.config({ path: ".env" });

export interface Config {
  port: number;
  dbFolder: string;
  jwtSecret: string;
}

const config: Config = {
  port: +(process.env.NODE_PORT || 3000),
  dbFolder: process.env.DB_FOLDER || "db_mock",
  jwtSecret: process.env.JWT_SECRET || "superSecret12#",
};

export { config };
