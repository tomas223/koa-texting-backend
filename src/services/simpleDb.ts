import fs from "node:fs";
import path from "path";
import AsyncLock from "async-lock";
import { config } from "../config";

type Value = string | Record<string, string | number | boolean>;

type DbModel<T extends Value> = {
  value: T | null;
  count: number;
};

const EMPTY_DB_DATA: DbModel<null> = { value: null, count: 0 };

export default class DatabaseService<T extends Value> {
  protected dbPath: string;
  protected lock: AsyncLock;

  private initDb() {
    fs.stat(this.dbPath, (err) => {
      if (err) {
        console.log(`Creating DB: ${this.dbPath}`);
        fs.writeFile(this.dbPath, JSON.stringify(EMPTY_DB_DATA), (err) => {
          console.error(`"Error Creating DB: ${this.dbPath}`, err);
        });
      }
    });
  }

  constructor(dbName: string) {
    this.dbPath = path.join(config.dbFolder, `${dbName}.json`).toString();
    this.lock = new AsyncLock();
    this.initDb();
  }

  private async readFile() {
    return (await fs.promises.readFile(this.dbPath, {})).toString();
  }

  private async writeFile(data: string) {
    return await fs.promises.writeFile(this.dbPath, data);
  }

  /**
   * Get last stored data with count of writing to DB
   * @returns object DbModel<T>
   */
  public async getLast(): Promise<DbModel<T>> {
    const lastRaw = await this.readFile();
    return JSON.parse(lastRaw);
  }

  /**
   * Save value as last message and increment count
   * AsyncLock is created to ensure atomicity
   * @param value to store to DB
   * @returns stored value in DbModel<T>
   */
  public async saveLast(value: T): Promise<DbModel<T>> {
    const record = await this.lock.acquire(this.dbPath, async () => {
      const { count } = await this.getLast();
      const newRecord = { value, count: count + 1 };
      await this.writeFile(JSON.stringify(newRecord));
      return newRecord;
    });

    return record;
  }
}
