import Joi, { Schema } from "joi";
import { Context, Next } from "koa";

const messageSchema = Joi.object({
  message: Joi.string().min(3).required(),
  from: Joi.string().min(3).required(),
  to: Joi.string().min(3).required(),
});

const BodyValidator = (schema: Schema) => async (ctx: Context, next: Next) => {
  try {
    await schema.validateAsync(ctx.request.body);
  } catch (err) {
    ctx.throw(400, err.message);
  }
  await next();
};

const messageValidator = BodyValidator(messageSchema);

export { messageValidator };
