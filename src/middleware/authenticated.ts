import { Context, Next } from "koa";
import compose from "koa-compose";
import jwt from "jsonwebtoken";

import { config } from "../config";
import User from "../models/user";

const secret = config.jwtSecret;

const authenticated = async (ctx: Context, next: Next) => {
  if (!ctx.headers.authorization) ctx.throw(403, "No token.");

  const token = ctx.headers.authorization.split(" ")[1];

  try {
    ctx.jwtPayload = jwt.verify(token, secret);
  } catch (err) {
    ctx.throw(err.status || 403, err.text);
  }

  await next();
};

const isAdmin = async (ctx: Context, next: Next) => {
  const { sub } = ctx.jwtPayload;
  if (!User.isAdmin(sub)) ctx.throw(403, "User doesn't have 'ADMIN' role");

  await next();
};

const authenticatedAdmin = compose([authenticated, isAdmin]);

export { authenticated, authenticatedAdmin };
