# Messaging API homework

## Start dev server

```bash
# install deps
npm i

# start dev server
make dev
# or
npm run watch-server
```

## How to use

First login

```bash
# admin
curl -X POST http://localhost:3000/login \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data username=admin \
  --data password=secret

# user
curl -X POST http://localhost:3000/login \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data username=user \
  --data password=secret
```

Copy JWT token and use it in further requests

```bash
# stats
curl localhost:3000/stats \
    --header 'Authorization: Bearer <paste_token_here>'

# create message
curl -X POST localhost:3000/message \
  --header "Content-Type: application/json" \
  -d '{"message":"Howdy ho","from":"Tomas","to":"Alex"}' \
  --header 'Authorization: Bearer <paste_token_here>'
```
